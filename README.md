Dependencies Version
===

* **Rails**: 5.0.2 (Ubuntu and Windows)
* **Ruby**: 2.4.0 (Ubuntu), 2.3.3 (Windows)
* **Node**: 7.7.2 (Ubuntu), 6.10.0 (Windows)
* **NPM**: 4.1.2 (Ubuntu), 3.10.10 (Windows)

<br />

Installing Phrogress
===

## Ubuntu

### Setup Git and Repository

**1. Install Git**

  ```
  sudo apt-get install git
  ```

**2. Configure Git**

  ```
  git config --global user.name "Your Name Here"
  git config --global user.email "your-gitlab-email@example.com"
  ```

**3. Clone Repository**

  ```
  git clone https://gitlab.com/PPL2017csui/PPLD2.git
  cd PPLD2
  ```
<br />

### Install RVM and Ruby

**1. Install Curl**

  ```
  sudo apt-get install curl
  ```

**2. RVM**

  ```
  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
  \curl -sSL https://get.rvm.io | bash -s stable
  source ~/.rvm/scripts/rvm
  echo "source ~/.rvm/scripts/rvm" >> .bashrc
  ```

  Then run `rvm requirements` and follow the instructions
  
  Note: hkp://keys.gnupg.net can sometimes timeout, please notice the output

**3. Ruby 2.4.0**

  ```
  rvm install 2.4.0
  ```
<br />

### Setup and Run Phrogress

**1. Run Setup Script**

  ```
  ./scripts/setup.sh
  ```

**2. Update `.env` file**

  Make any changes on `.env` file if required

**3. Run Phrogress**

  ```
  rails server
  ```

**4. Access it at `http://localhost:3000`**

<br />

## Windows

### Setup Git and Repository

**1. Install Git**

  Download Git from [https://git-scm.com/download/win](https://git-scm.com/download/win) then follow the instructions

**2. Add Git to Environment Variables**

  Make sure Git is added to your environment variables

**3. Configure Git**

  ```
  git config --global user.name "Your Name Here"
  git config --global user.email "your-gitlab-email@example.com"
  ```

**4. Clone Repository**

  ```
  git clone https://gitlab.com/PPL2017csui/PPLD2.git
  cd PPLD2
  ```
<br />

### Install Ruby

**1. Download Ruby v2.3.3**

  Download Ruby v2.3.3 from [https://rubyinstaller.org](https://rubyinstaller.org) then follow the instructions

**2. Add Ruby to Environment Varibles**

  Make sure Ruby is added to your environment variables  

<br />

### Setup and Run Phrogress

**1. Download MongoDB**

  Download MongoDB from [https://www.mongodb.com/download-center](https://www.mongodb.com/download-center) then follow the instructions

**2. Start MongoDB**

  Run `mongod.exe`
  
  If you want to find the details you can read on [Install MongoDB Community Edition on Windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)

**3. Download Node.js v6.10.0**

  Download Node.js v6.10.0 from [https://nodejs.org/en/download/](https://nodejs.org/en/download/) then follow the instructions

**4. Install Bundler**

  ```
  gem install bundler
  ```

**5. Install Gems**

  ```
  bundle install
  ```

**6. Install Node Packages**

  ```
  npm install
  ```

**7. Copy `env` File**

  Copy `env.sample` file in root project directory to `.env` then make any changes if required

**8. Run Phrogress**

  ```
  rails server
  ```

**9. Access it at `http://localhost:3000`**

<br />

Setup Linter
===

## ESLint

1. Open RubyMine
2. Go to File > Settings
3. Search "ESLint"
4. Check the "Enable" box
5. Make sure "Node interpreter" field contains path to executable Node, such as `/usr/local/bin/node`
6. Make sure "ESLint package" filled with `<path_to_root_project>/node_modules/eslint`
7. Choose "Automatic search" in "Configuration file" panel
8. Click "Apply" then "OK"

<br />

Generating Documentation
===

Make sure `yard` is installed, and run the command `yard doc` at the root of this repository.
The documentation will be available at `doc/index.html`.

Useful Commands
===

**Run React Testing**

  ```
  npm test
  ```
  
**Clear Asset Pipeline Cache**

  ```
  rake tmp:cache:clear
  ```
  
***Run ESLint on jsx codes***
    ```
    ./node_modules/eslint/bin/eslint.js --ext .jsx ./
    ```
