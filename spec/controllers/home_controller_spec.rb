require 'spec_helper'
require 'controllers/controller_spec_helper'

describe HomeController, :type => :controller do
  include ControllerSpecHelper
  include_context 'vcr configuration'

  describe 'GET #index' do

    context 'successfully connected to the Phabricator host' do
      before do
        VCR.use_cassette 'home_controller_index', :record => :new_episodes, :match_requests_on => [:method, :uri, :body] do
          get :index
        end
      end

      it 'renders the :index view' do
        expect(response).to render_template('index')
      end

      it 'fetch projects list' do
        expect(assigns(:project_list)).not_to eql(nil)
      end

      it 'fetch task list' do
        expect(assigns(:task_list)).not_to eql(nil)
      end

      it 'make a hash that match task by project' do
        expect(assigns(:task_by_project)).not_to eql(nil)
        expect(assigns(:task_by_project)).to be_a Hash
      end

      it 'parse project list to project_data' do
        expect(assigns(:project_data)).not_to eql(nil)
      end
    end

    # context 'unsuccessfully connect to the Phabricator host' do
    #   before do
    #     stub_request(:any, /#{conduit.host}/).to_timeout # How to do this from controller??
    #   end
    #
    #   it 'should make an error message' do
    #     expect (assigns(:error_message)).to eql('Error connecting to Phabricator')
    #   end
    # end


  end

end