require 'rails_helper'
require 'controllers/controller_spec_helper'

RSpec.describe ProjectDetailsController, type: :controller do
  include ControllerSpecHelper
  include_context 'vcr configuration'

  describe "GET #index" do
    it "returns http success" do
      VCR.use_cassette('index') do
        get :index, :id => 2
        expect(response).to have_http_status(:success)
      end
    end

    it "render correct project" do
      VCR.use_cassette('index') do
        get :index, :id => 2
        expect(response).to render_template("index")
      end
    end
  end
end
