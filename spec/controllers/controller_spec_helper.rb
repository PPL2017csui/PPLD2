# RbCAW tests dependencies
require 'rspec/rails'
require 'webmock/rspec'
require 'vcr'

module ControllerSpecHelper
  shared_context 'vcr configuration' do
    before do
      VCR.configure do |c|
        c.cassette_library_dir = 'spec/fixtures/controller_cassettes'
        c.hook_into :webmock
      end
    end
  end
end
