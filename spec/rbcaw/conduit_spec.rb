require 'rbcaw/conduit'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::Conduit, '#initialize' do
  include RbCAWSpecHelper
  include_context 'vcr configuration'

  it 'should initialize properly' do
    expect(conduit.host).to eql(conduit_host)

    expect(conduit.maniphest).not_to eql(nil)
    expect(conduit.project).not_to eql(nil)
    expect(conduit.sprint).not_to eql(nil)
    expect(conduit.user).not_to eql(nil)
    expect(conduit.util).not_to eql(nil)

    expect(conduit).to respond_to(:request)
  end

  context 'while the Phabricator host is not responding' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a ConnectionTimeoutError' do
      expect { conduit.util.ping }.to raise_error(RbCAW::ConnectionTimeoutError)
    end
  end

  context 'while the Phabricator host is down' do
    before do
      stub_request(:any, /#{conduit.host}/).to_raise(Errno::ECONNREFUSED)
    end

    it 'should not be able to retrieve info and raised a ConnectionRefusedError' do
      expect { conduit.util.ping }.to raise_error(RbCAW::ConnectionRefusedError)
    end
  end

  context 'with the network connection aborted' do
    before do
      stub_request(:any, /#{conduit.host}/).to_raise(Errno::ECONNABORTED)
    end

    it 'should not be able to retrieve info and raised a ConnectionAbortedError' do
      expect { conduit.util.ping }.to raise_error(RbCAW::ConnectionAbortedError)
    end
  end

  context 'with the network connection reset' do
    before do
      stub_request(:any, /#{conduit.host}/).to_raise(Errno::ECONNRESET)
    end

    it 'should not be able to retrieve info and raised a ConnectionResetError' do
      expect { conduit.util.ping }.to raise_error(RbCAW::ConnectionResetError)
    end
  end
end