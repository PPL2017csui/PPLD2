require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::SprintConduitFetcher, '#info' do
  include RbCAWSpecHelper
  include_context 'vcr configuration'

  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'sprint_info', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @task_sprint_info = conduit.sprint.info(test_task.id)
      @project_sprint_info = conduit.sprint.get_project_start_end_date(test_project.phid)
    end

    it 'should retrieve the task\'s sprint info properly' do
      expect(@task_sprint_info).to eq(test_task.sprint_info)
    end

    it 'should retrieve the project\'s sprint info properly' do
      expect(@project_sprint_info).to eq(test_project.sprint_info)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while the Phabricator host is not responding' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a ConnectionTimeoutError' do
      expect { conduit.sprint.info(test_task.id) }.to raise_error(RbCAW::ConnectionTimeoutError)
    end
  end
end