require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::TaskSprintInfoLazyModel do
  include RbCAWSpecHelper
  include_context 'vcr configuration'

  context 'with connected network' do
    before do
      VCR.insert_cassette 'sprint_info_model', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @task = RbCAW::TaskSprintInfoLazyModel.new(test_task.id, conduit: conduit)
    end

    it 'should have all accessor usable and returning right values' do
      expect(@task.id).to eql(test_task.id)
      expect(@task.point).to eql(test_task.sprint_info.point)
      expect(@task.is_closed).to eql(test_task.sprint_info.is_closed)
    end

    after do
      VCR.eject_cassette
    end
  end
end

RSpec.describe RbCAW::ProjectSprintInfoLazyModel do
  include RbCAWSpecHelper
  include_context 'vcr configuration'

  context 'with connected network' do
    before do
      VCR.insert_cassette 'sprint_info_model', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @sprint_info = RbCAW::ProjectSprintInfoLazyModel.new(test_project.phid, conduit: conduit)
    end

    it 'should have all accessor usable and returning right values' do
      expect(@sprint_info).to eq(test_project.sprint_info)
    end

    after do
      VCR.eject_cassette
    end
  end
end