require_relative '../fetchers/base_conduit_fetcher'
require_relative '../models/search/project_search_result'

module RbCAW
  # Fetcher class for Project-related API calls
  class ProjectConduitFetcher < BaseConduitFetcher

    # List, query, or search for Phabricator Projects. Constraints can be
    # applied to search for specific projects (or leaved empty to get all
    # projects). For more detailed information, please refer to
    # `/conduit/method/project.search/` at your Phabricator.
    #
    # @param (see BaseConduitFetcher#search)
    #
    # @param constraints [Hash{String => BasicObject}] search constraints that
    #   will narrow the search result.
    # @option constraints :ids [Array<Fixnum>] Search for objects with specific
    #   IDs
    # @option constraints :phids [Array<String>] Search for objects with
    #   specific PHIDs
    # @option constraints :name [String] Search for objects with specified name
    # @option constraints :members [Array<String>] Search for objects with
    #   specified members (PHID)
    # @option constraints :watchers [Array<String>] Search for objects with
    #   specified watchers (PHID)
    # @option constraints :isMilestone [TrueClass, FalseClass] Pass true to find
    #   only milestones, or false to omit
    # @option constraints :icons [Array<String>] Search for objects with
    #   specified icons
    # @option constraints :colors [Array<String>] Search for objects with
    #   specified colors
    # @option constraints :parents [Array<String>] Find direct subprojects of
    #   specified parents (PHID)
    # @option constraints :ancestors [Array<String>] Find all subprojects
    #   beneath specified (PHID)
    # @option constraints :issprint [Boolean] Project Is Sprint
    #
    # @param attachments [Hash{String => TrueClass, FalseClass}] additional
    #   attachments that will be included on the search results.
    # @option attachments :members [TrueClass, FalseClass] If true, include
    #   project members
    # @option attachments :watchers [TrueClass, FalseClass] If true, include
    #   project watchers `Warning: currently not supported`
    # @option attachments :ancestors [TrueClass, FalseClass] If true, include
    #   project ancestors `Warning: currently not supported`
    #
    # @param order [String, Array<String>] specify order of search result. For
    #   more detailed sorting please refer to `/conduit/method/project.search/`.
    #   Possible (builtin) values are:
    #   - `name` : Name
    #   - `newest` : Creation (Newest First)
    #   - `oldest` : Creation (Oldest First)
    #   - `startdate` : Sprint Start Date
    #   - `-startdate` : Sprint Start Date (Reversed)
    #   - `enddate` : Sprint End Date
    #   - `-enddate` : Sprint End Date (Reversed)
    #   - `issprint` : Is Sprint
    #   - `-issprint` : Is Sprint (Reversed)
    #
    # @return [ProjectSearchResult] the search result
    def search(query_key: nil, constraints: {}, attachments: {},
               order: nil, before: nil, after: nil, limit: 100)
      json_obj = conduit_search(method: 'project.search', query_key: query_key,
                                constraints: constraints, attachments: attachments,
                                order: order, before: before, after: after, limit: limit)

      return ProjectSearchResult.new(@conduit, json_obj)
    end

    # Get a Phabricator Project by its PHID
    #
    # @param phid [String] the Project's PHID
    # @param request_members [TrueClass, FalseClass] whether to include
    #   Project's member on the resulting project or not. Default is `true`.
    # @return [ProjectLazyModel] the Project with specified PHID
    #
    # @raise [ArgumentError] if PHID is nil
    # @raise [ResourceError] if Project with specified PHID is not found
    def get(phid:, request_members: true)
      return super(
        phid: phid,
        attachments: (request_members ? { members: true } : {})
      )
    end
  end
end
