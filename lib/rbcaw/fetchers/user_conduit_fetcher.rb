require_relative '../fetchers/base_conduit_fetcher'
require_relative '../models/user_lazy_model'
require_relative '../models/search/user_search_result'

module RbCAW
  # Fetcher class for User-related API calls
  class UserConduitFetcher < BaseConduitFetcher
    # Get information about currently logged-in user
    #
    # @param oauth_session_token [String] OAuth token of the logged-in user.
    #   `Warning: this feature is work in progress`
    # @return [UserLazyModel] the logged-in user
    def whoami(oauth_session_token: nil)
      json_obj = @conduit.request(
        'user.whoami',
        session_token: oauth_session_token
      )

      user = UserLazyModel.new(json_obj['result']['phid'])
      user.manual_init(
        json_obj['result']['userName'],
        json_obj['result']['realName'],
        json_obj['result']['image'],
        json_obj['result']['roles']
      )

      return user
    end

    # List, query, or search for Phabricator users. Constraints can be applied
    # to search for specific tasks (or leaved empty to get all tasks). For more
    # detailed information, please refer to `/conduit/method/user.search/` at
    # your Phabricator.
    #
    # @param (see BaseConduitFetcher#search)
    #
    # @param constraints [Hash{String => BasicObject}] search constraints that
    #   will narrow the search result.
    # @option constraints :ids [Array<Fixnum>]
    #   Search for objects with specific IDs
    # @option constraints :phids [Array<String>]
    #   Search for objects with specific PHIDs (User PHID)
    # @option constraints :usernames [Array<String>]
    #   Find users by exact username
    # @option constraints :nameLike [String]
    #   Find users whose usernames contain a substring
    # @option constraints :isAdmin [TrueClass, FalseClass]
    #   Pass true to find only administrators, or false to omit administrators
    # @option constraints :isDisabled [TrueClass, FalseClass]
    #   Pass true to find only disabled users, or false to omit disabled users
    # @option constraints :isBot [TrueClass, FalseClass]
    #   Pass true to find only bots, or false to omit bots
    # @option constraints :isMailingList [TrueClass, FalseClass]
    #   Pass true to find only mailing lists, or false to omit mailing lists
    # @option constraints :needsApproval [TrueClass, FalseClass]
    #   Pass true to find only users awaiting administrative approval, or false
    #   to omit these users
    # @option constraints :createdStart [Fixnum]
    #   Find user accounts created after a given time
    # @option constraints :createdEnd [Fixnum]
    #   Find user accounts created before a given time
    #
    # @param attachments [Hash{String => TrueClass, FalseClass}] additional
    #   attachments that will be included on the search results. Currently,
    #   there are no attachments supported for {UserLazyModel}.
    #
    # @param order [String, Array<String>] specify order of search result.
    #   For more detailed sorting please refer to `/conduit/method/user.search/`.
    #   Possible (builtin) values are:
    #   - `newest` : Creation (Newest First)
    #   - `oldest` : Creation (Oldest First)
    #
    # @return [UserSearchResult] the search result
    def search(query_key: nil, constraints: {}, attachments: {},
               order: nil, before: nil, after: nil, limit: 100)
      json_obj = conduit_search(method: 'user.search', query_key: query_key,
                                constraints: constraints, attachments: attachments,
                                order: order, before: before, after: after, limit: limit)

      return UserSearchResult.new(@conduit, json_obj)
    end

    # Get a Phabricator User by its PHID or username
    #
    # @param phid [String] the User's PHID
    # @param username [String] the User's username
    # @return [UserLazyModel] the Project with specified PHID
    #
    # @raise [ArgumentError] if PHID and username is nil
    # @raise [ResourceError] if User with specified PHID is not found
    def get(phid: nil, username: nil)
      if phid.nil? && username.nil?
        raise ArgumentError, 'One of PHID or username should not be nil'
      end

      constraints = {}

      unless phid.nil?
        constraints['phids'] = [phid]
      end

      unless username.nil?
        constraints['usernames'] = [username]
      end

      search_result = search(constraints: constraints, limit: 1)

      if search_result.results.empty?
        raise ResourceError, "User with PHID \"#{phid}\" or username \"#{username}\" is not found"
      end

      return search_result.results[0]
    end
  end
end
