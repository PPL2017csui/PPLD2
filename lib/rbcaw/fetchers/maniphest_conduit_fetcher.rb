require_relative '../fetchers/base_conduit_fetcher'
require_relative '../models/search/maniphest_search_result'

module RbCAW
  # Fetcher class for Maniphest Task-related API calls
  class ManiphestConduitFetcher < BaseConduitFetcher
    # List, query, or search for Phabricator Maniphest tasks. Constraints can be
    # applied to search for specific tasks (or leaved empty to get all tasks).
    # For more detailed information, please refer to
    # `/conduit/method/maniphest.search/` at your Phabricator.
    #
    # @param (see BaseConduitFetcher#search)
    #
    # @param constraints [Hash{String => BasicObject}] search constraints that
    #   will narrow the search result. Refer to `/conduit/method/maniphest.search/`
    #   for detailed constraints.
    # @option constraints :ids [Array<Fixnum>] Search for objects with specific IDs
    # @option constraints :phids [Array<String>] Search for objects with specific
    #   PHIDs (Task PHID)
    # @option constraints :assigned [Array<String>] Search for tasks owned by a
    #   user from a list (User PHID)
    # @option constraints :authorPHIDs [Array<String>] Search for tasks with
    #   given authors (User PHID)
    # @option constraints :statuses [Array<String>] Search for tasks with given
    #   statuses
    # @option constraints :priorities [Array<Fixnum>] Search for tasks with
    #   given priorities
    # @option constraints :fulltext [String]
    # @option constraints :hasParents [TrueClass, FalseClass]
    # @option constraints :hasSubtasks [TrueClass, FalseClass]
    # @option constraints :parentIDs [Array<Fixnum>]
    # @option constraints :subtaskIDs [Array<Fixnum>]
    # @option constraints :createdStart [Fixnum]
    # @option constraints :createdEnd [Fixnum]
    # @option constraints :modifiedStart [Fixnum]
    # @option constraints :modifiedEnd [Fixnum]
    # @option constraints :subscribers [Array<String>] Search for objects with
    #   certain subscribers (User PHID)
    # @option constraints :projects [Array<String>] Search for objects tagged
    #   with given projects (Project PHID)
    # @option constraints :storypoints [Fixnum] Estimated story points for this
    #   task
    #
    # @param attachments [Hash{String => TrueClass, FalseClass}] additional
    #   attachments that will be included on the search results.
    # @option attachments :projects [TrueClass, FalseClass] If true, include
    #   related projects
    # @option attachments :subscribers [TrueClass, FalseClass] If true, include
    #   task subscribers
    #   `Warning: currently not supported`
    # @option attachments :columns [TrueClass, FalseClass] If true, include
    #   task columns
    #   `Warning: currently not supported`
    #
    # @param order [String, Array<String>] specify order of search result. For
    #   more detailed sorting please refer to `/conduit/method/maniphest.search/`.
    #   Possible (builtin) values are:
    #   - `Key` : Description
    #   - `priority` : Priority
    #   - `updated` : Date Updated (Latest First)
    #   - `outdated` : Date Updated (Oldest First)
    #   - `newest` : Creation (Newest First)
    #   - `oldest` : Creation (Oldest First)
    #   - `title` : Title
    #   - `storypoints` : Story Points
    #   - `-storypoints` : Story Points (Reversed)
    #
    # @return [ManiphestSearchResult] the search result
    def search(query_key: nil, constraints: {}, attachments: {},
               order: nil, before: nil, after: nil, limit: 100)
      json_obj = conduit_search(method: 'maniphest.search', query_key: query_key,
                                constraints: constraints, attachments: attachments,
                                order: order, before: before, after: after,
                                limit: limit)

      return ManiphestSearchResult.new(@conduit, json_obj)
    end

    # Get a Maniphest Task by its PHID
    #
    # @param phid [String] the Task's PHID
    # @param request_projects [TrueClass, FalseClass] whether to include Projects containing
    #   this task or not. Default is `true`.
    # @return [TaskLazyModel] the Project with specified PHID
    #
    # @raise [ArgumentError] if PHID is nil
    # @raise [ResourceError] if Task with specified PHID is not found
    def get(phid:, request_projects: true)
      return super(
        phid: phid,
        attachments: (request_projects ? { projects: true } : {})
      )
    end
  end
end