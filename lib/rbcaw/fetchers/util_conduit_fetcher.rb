require_relative '../fetchers/base_conduit_fetcher'

module RbCAW
  # Fetcher class for general Conduit API calls
  class UtilConduitFetcher < BaseConduitFetcher

    # Get Phabricator host information. Used to check network connection
    # to the host.
    #
    # @return [JSON] Phabricator host information
    def ping
      return @conduit.request('conduit.ping')
    end
  end
end
