require_relative '../fetchers/base_conduit_fetcher'
require_relative '../models/sprint_info_lazy_model'

module RbCAW
  # Fetcher class for Sprint-related API calls
  class SprintConduitFetcher < BaseConduitFetcher
    # Get sprint information of a Maniphest Task
    #
    # @param task_id [Fixnum] the id (not PHID) of the task
    # @return [TaskSprintInfoLazyModel] the sprint information of the task
    def info(task_id)
      params = {
        task_id: task_id
      }

      json_obj = @conduit.request('sprint.info', params: params)

      sprint_info = TaskSprintInfoLazyModel.new(task_id)
      sprint_info.manual_init(
        json_obj['result']['points'].to_i,
        json_obj['result']['isClosed']
      )

      return sprint_info
    end

    # Get sprint information of a Project
    #
    # @param phid [String] the PHID of the project
    # @return [ProjectSprintInfoLazyModel] the sprint information of the project
    def get_project_start_end_date(phid)
      params = {
        project: phid
      }

      json_obj = @conduit.request('sprint.getstartenddates', params: params)

      sprint_info = ProjectSprintInfoLazyModel.new(phid)
      sprint_info.manual_init(
        json_obj['result']['start'], json_obj['result']['end']
      )

      return sprint_info
    end
  end
end
