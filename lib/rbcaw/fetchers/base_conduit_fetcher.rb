module RbCAW
  # Base class for Conduit API fetchers. Methods defined in this class should
  # not be called directly
  #
  # @attr_reader conduit [Conduit] The Conduit instance this fetcher will use to
  #   make HTTP requests
  class BaseConduitFetcher
    attr_reader :conduit

    # Initialize a fetcher instance
    #
    # @param conduit [Conduit] the Conduit instance this fetcher will use to
    #   make HTTP requests
    def initialize(conduit)
      @conduit = conduit
    end

    # @abstract
    # Do a Phabricator object search
    #
    # @param query_key [String] builtin search query, default is `all` which
    #   will list all projects. Other values are `joined` and `active`. Query
    #   defined by `query_key` will still be overridden by `constraints`.
    #
    # @param constraints [Hash{String => BasicObject}] search constraints that
    #   will narrow the search result.
    #
    # @param attachments [Hash{String => TrueClass, FalseClass}] additional
    #   attachments that will be included on the search results.
    #
    # @param order [String, Array<String>] specify order of search result.
    #
    # @param before [Fixnum] get the previous page of results. The value for
    #   this parameter is acquired from {Cursor#before} of an existing
    #   {SearchResult}.
    #
    # @param after [Fixnum] get the next page of results. The value for this
    #   parameter is acquired from {Cursor#after} of an existing {SearchResult}.
    #
    # @param limit [Fixnum] limit the amount of projects searched per page.
    #   Default value is 100.
    def search(query_key: nil, constraints: {}, attachments: {},
               order: nil, before: nil, after: nil, limit: 100)
      raise NoMethodError, 'Method search not defined'
    end

    # Get a Phabricator object by its PHID
    #
    # @param phid [String] the object's PHID
    # @param attachments [Hash] the attachments that will be included on API
    #   call response
    # @return [ConduitLazyModel] the object with specified PHID
    #
    # @raise [ArgumentError] if PHID is nil
    # @raise [ResourceError] if object with specified PHID is not found
    def get(phid:, attachments: {})
      if phid.nil?
        raise ArgumentError, 'PHID should not be nil'
      end

      search_result = search(
        constraints: { phids: [phid] },
        attachments: attachments,
        limit: 1
      )

      if search_result.results.empty?
        raise ResourceError, "Resource with PHID \"#{phid}\" is not found"
      end

      return search_result.results[0]
    end

    protected

    def conduit_search(method: nil, query_key: nil, constraints: {}, attachments: {},
                       order: nil, before: nil, after: nil, limit: 100)
      if method.nil?
        raise ArgumentError, 'Conduit method name cannot be null'
      end

      params = {
        queryKey: query_key,
        constraints: constraints,
        attachments: attachments,
        order: order,
        before: before,
        after: after,
        limit: limit
      }

      return @conduit.request(method, params: params)
    end
  end
end