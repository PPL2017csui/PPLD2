require 'net/http'
require 'uri'
require 'json'

require_relative './errors'
require_relative './fetchers/maniphest_conduit_fetcher'
require_relative './fetchers/project_conduit_fetcher'
require_relative './fetchers/sprint_conduit_fetcher'
require_relative './fetchers/user_conduit_fetcher'
require_relative './fetchers/util_conduit_fetcher'

# Namespace for classes on Ruby Conduit API Wrapper
module RbCAW
  # The Conduit class provides convenient access to Phabricator's API, Conduit.
  # Instances of this class are the gateway to interacting with Conduit through
  # RbCAW.
  #
  # @attr_reader host [String] The URL of Phabricator host
  #
  # @attr_reader maniphest [ManiphestConduitFetcher] The Maniphest helper for
  #   API calls involving Maniphest tasks
  #
  # @attr_reader project [ProjectConduitFetcher] The Project helper for API
  #   calls involving projects
  #
  # @attr_reader sprint [SprintConduitFetcher] The Sprint helper for API calls
  #   involving sprints information
  #
  # @attr_reader user [UserConduitFetcher] The User helper for API calls
  #   involving users
  #
  # @attr_reader util [UtilConduitFetcher] The Utility helper for general
  #   Conduit API calls
  class Conduit
    attr_reader :host, :maniphest, :project, :sprint, :user, :util

    # Initialize a Conduit instance.
    #
    # @param host [String] the URL of Phabricator host
    #
    # @param api_token [String] the API token generated from
    #   'Conduit API Tokens' menu on user setting
    #
    # @return [Conduit] the Conduit class instance
    def initialize(host, api_token)
      @host = host
      @api_token = api_token
      @uri = URI.parse(host)

      @maniphest = ManiphestConduitFetcher.new(self)
      @project = ProjectConduitFetcher.new(self)
      @sprint = SprintConduitFetcher.new(self)
      @user = UserConduitFetcher.new(self)
      @util = UtilConduitFetcher.new(self)
    end

    # Make an API call via POST to the Phabricator host. This method should not
    # be called directly, use {maniphest}, {project}, {sprint}, {user}, or
    # {util} instead.
    #
    # @param method [String] the API method called. This value depends on API
    #   methods defined at `/conduit` on the Phabricator host
    #
    # @param params [Hash] parameters for the API call
    #
    # @param session_token [String] optional session token from OAuth
    #   authentication. If this parameter is specified, it will be used instead
    #   of API token supplied from the Conduit instance. `Warning: This feature
    #   is still work in progress`
    #
    # @param output [String] optional output format, the default is `json`.
    #   Other possible value is `human` which will make Conduit returns an HTML
    #   instead, thus will make this method raise an error.
    #
    # @return [JSON] the API call result
    #
    # @raise [TimeoutError] if the request timed out
    # @raise [RequestError] if Conduit returned an error
    def request(method, params: {}, session_token: nil, output: 'json')
      http = Net::HTTP.new(@uri.host, @uri.port)
      http.use_ssl = (@uri.scheme == 'https')

      request = Net::HTTP::Post.new("/api/#{method}")

      params['__conduit__'] = {
        token: (!session_token.nil? ? session_token : @api_token)
      }

      form_data = {
        __conduit__: '1',
        output: output,
        params: params.to_json.to_s
      }

      request.set_form_data(form_data)

      begin
        response = http.request(request)
      rescue Timeout::Error
        raise ConnectionTimeoutError, "Request to Conduit host (#{@host}) timed out"
      rescue Errno::ECONNREFUSED
        raise ConnectionRefusedError, "Request to Conduit host (#{@host}) refused"
      rescue Errno::ECONNABORTED
        raise ConnectionAbortedError, "Request to Conduit host (#{@host}) aborted"
      rescue Errno::ECONNRESET
        raise ConnectionResetError, "Request to Conduit host (#{@host}) reset"
      end

      json_response = JSON.parse(response.body)

      unless json_response['error_code'].nil?
        raise RequestError, "#{json_response['error_code']}: #{json_response['error_info']}"
      end

      return json_response
    end
  end
end