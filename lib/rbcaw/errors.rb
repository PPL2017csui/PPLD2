module RbCAW
  # Error raised if the Phabricator host returned an error following
  # an API call
  class RequestError < StandardError
  end

  # Error raised if there's a problem when fetching variable values
  # of a Lazy Model
  class ModelError < StandardError
  end

  # Error raised if a `get` method failed to find the resource with
  # specified ID / PHID
  class ResourceError < StandardError
  end

  # Generic error raised if the connection to the Phabricator host
  # failed
  class ConnectionError < StandardError
  end

  # Error raised if the connection to the Phabricator host timed out
  class ConnectionTimeoutError < ConnectionError
  end

  # Error raised if the connection to the Phabricator host is refused
  class ConnectionRefusedError < ConnectionError
  end

  # Error raised if the connection to the Phabricator host is aborted
  class ConnectionAbortedError < ConnectionError
  end

  # Error raised if the connection to the Phabricator host is reset
  class ConnectionResetError < ConnectionError
  end

  # Error raised if an API call is lacking some required arguments
  class ArgumentError < StandardError
  end
end
