module RbCAW
  # @abstract
  # Base class for Phabricator object models. This class is a lazy model,
  # which make it possible to instantiate an object without fetching from
  # the Phabricator host. Additional variable values will be fetched as
  # needed.
  # This class will not fetch additional information from the host unless
  # its variables are accessed.
  #
  # @attr_reader phid [String] The Phabricator ID of this object
  class ConduitLazyModel
    attr_reader :phid

    # Initialize a Phabricator object. The `conduit` param is only needed
    # if this object will not be manually initialized.
    # @see #manual_init
    #
    # @param phid [String]      the Phabricator ID of this object
    # @param conduit [Conduit]  the Conduit instance that will be used to
    #   fetch additional information
    def initialize(phid, conduit: nil)
      @fetched = false

      @phid = phid
      @conduit = conduit
    end

    # Check whether this model's variables are already fetched or not, and
    # fetch those if they haven't.
    #
    # @raise [ModelError] if the Conduit instance is nil
    # @raise [ResourceError] if the object specified by {phid} is not found
    def check_fetched
      return if @fetched

      raise ModelError, 'Conduit helper is nil' if @conduit.nil?

      begin
        fetch
      rescue ResourceError => e
        raise ModelError, "Failed to fetch information for #{self.class} with PHID #{phid}: #{e.message}"
      end
    end

    # @abstract
    # Manually initialize this object by supplying all the instance variables
    def manual_init
      raise NoMethodError, 'Initialization method not defined'
    end

    # @abstract
    # Fetch and assign the values of this object's variables
    def fetch
      raise NoMethodError, 'Fetch method not defined'
    end

    # Check whether two {ConduitLazyModel} is the same or not by comparing their
    # variables. This method will fetch variables for both object.
    #
    # @param o [BasicObject] the object to be compared
    # @return [TrueClass, FalseClass] true if the objects' variables are equal
    def ==(o)
      return false unless o.is_a? ConduitLazyModel

      check_fetched
      o.check_fetched

      self.instance_variables.each { |var|
        next if var == :@conduit

        unless (self.instance_variable_get(var).eql? o.instance_variable_get(var)) ||
               (self.instance_variable_get(var) == o.instance_variable_get(var))
          return false
        end
      }

      return true
    end
  end
end