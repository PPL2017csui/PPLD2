require_relative '../models/conduit_lazy_model'

module RbCAW
  # Model class for a Sprint information of a Maniphest task. This class
  # is a lazy model, which make it possible to instantiate an object without
  # fetching from the Phabricator host. Additional variable values will be
  # fetched as needed.
  # This class will not fetch additional information from the host unless
  # its variables are accessed.
  #
  # @!attribute [r] id
  #   @return [Fixnum] the id of the associated task
  #
  # @!attribute [r] point
  #   @return [Fixnum] the point of the associated task
  #
  # @!attribute [r] is_closed
  #   @return [TrueClass, FalseClass] true if the the associated task has been
  #     closed
  class TaskSprintInfoLazyModel < ConduitLazyModel
    attr_reader :id

    # Initialize a Sprint information object by the ID of its associated task.
    # The `conduit` param is only needed if this object will not be manually
    # initialized.
    # @see #manual_init
    #
    # @param id [Fixnum]        the id of the associated task
    # @param conduit [Conduit]  the Conduit instance that will be used to
    #   fetch additional information
    def initialize(id, conduit: nil)
      @fetched = false

      @id = id
      @conduit = conduit
    end

    # Manually initialize this object by supplying all the instance variables
    #
    # @param point [Fixnum] the point of the associated task
    # @param is_closed [TrueClass, FalseClass] true if the the associated
    #   task has been closed
    def manual_init(point, is_closed)
      @point = point
      @is_closed = is_closed

      @fetched = true
    end

    # (see ConduitLazyModel#check_fetched)
    def check_fetched
      return if @fetched

      raise ModelError, 'Conduit helper is nil' if @conduit.nil?

      begin
        fetch
      rescue ResourceError => e
        raise ModelError, "Failed to fetch information for #{self.class} with id #{id}: #{e.message}"
      end
    end

    # Fetch and assign the values of this sprint information's variables
    def fetch
      sprint_info = @conduit.sprint.info(@id)

      manual_init(sprint_info.point, sprint_info.is_closed)
    end

    def point
      check_fetched
      return @point
    end

    def is_closed
      check_fetched
      return @is_closed
    end
  end

  # Model class for a Sprint information of a Phabricator project. This class
  # is a lazy model, which make it possible to instantiate an object without
  # fetching from the Phabricator host. Additional variable values will be
  # fetched as needed.
  # This class will not fetch additional information from the host unless
  # its variables are accessed.
  #
  # @!attribute [r] start_date
  #   @return [Fixnum] the sprint start date of this project
  #
  # @!attribute [r] end_date
  #   @return [Fixnum] the sprint end date of this project
  class ProjectSprintInfoLazyModel < ConduitLazyModel
    # Manually initialize this object by supplying all the instance variables
    #
    # @param start_date [Fixnum] the sprint start date of this project
    # @param end_date [Fixnum] the sprint end date of this project
    def manual_init(start_date, end_date)
      @start_date = start_date
      @end_date = end_date

      @fetched = true
    end

    # (see ConduitLazyModel#check_fetched)
    def check_fetched
      return if @fetched

      raise ModelError, 'Conduit helper is nil' if @conduit.nil?

      begin
        fetch
      rescue ResourceError => e
        raise ModelError, "Failed to fetch information for #{self.class} with id #{id}: #{e.message}"
      end
    end

    # Fetch and assign the values of this sprint information's variables
    def fetch
      sprint_info = @conduit.sprint.get_project_start_end_date(@phid)

      manual_init(sprint_info.start_date, sprint_info.end_date)
    end

    def start_date
      check_fetched
      return @start_date
    end

    def end_date
      check_fetched
      return @end_date
    end
  end
end
