require_relative '../models/conduit_lazy_model'
require_relative '../models/sprint_info_lazy_model'

module RbCAW
  # Model class for a Phabricator Maniphest task. This class is a lazy model,
  # which make it possible to instantiate an object without fetching from
  # the Phabricator host. Additional variable values will be fetched as
  # needed.
  # This class will not fetch additional information from the host unless
  # its variables are accessed.
  #
  # @!attribute [r] id
  #   @return [Fixnum] the id of this task
  #
  # @!attribute [r] name
  #   @return [String] the name of this task
  #
  # @!attribute [r] author
  #   @return [UserLazyModel] the author of this task
  #
  # @!attribute [r] owner
  #   @return [UserLazyModel] the author of this task
  #
  # @!attribute [r] status
  #   @return [String] the status of this task
  #
  # @!attribute [r] priority
  #   @return [Fixnum] the priority of this task
  #
  # @!attribute [r] projects
  #   @return [Array<ProjectLazyModel>] the projects associated with this task
  #
  # @!attribute [r] date_created
  #   @return [Fixnum] the date this task was created
  #
  # @!attribute [r] date_modified
  #   @return [Fixnum] the latest date this task was modified
  #
  # @!attribute [r] sprint_info
  #   @return [TaskSprintInfoLazyModel] the sprint-related information
  #   of this task
  class TaskLazyModel < ConduitLazyModel
    attr_reader :sprint_info

    # Manually initialize this object by supplying all the instance variables
    #
    # @param id [Fixnum] the id of this task
    # @param name [String] the name of this task
    # @param author [UserLazyModel] the author of this task
    # @param owner [UserLazyModel] the author of this task
    # @param status [String] the status of this task
    # @param priority [Fixnum] the priority of this task
    # @param projects [Array<ProjectLazyModel>] the projects associated with
    #   this task
    # @param date_created [Fixnum] the date this task was created
    # @param date_modified [Fixnum] the latest date this task was modified
    def manual_init(id, name, author, owner, status, priority,
                    projects, date_created, date_modified)
      @id = id
      @name = name
      @author = author
      @owner = owner
      @status = status
      @priority = priority
      @projects = projects
      @date_created = date_created
      @date_modified = date_modified

      @sprint_info = TaskSprintInfoLazyModel.new(@id, conduit: @conduit)

      @fetched = true
    end

    # Fetch and assign the values of this task's variables
    def fetch
      task = @conduit.maniphest.get(phid: @phid)

      manual_init(
        task.id, task.name, task.author, task.owner, task.status,
        task.priority, task.projects, task.date_created, task.date_modified
      )
    end

    def id
      check_fetched
      return @id
    end

    def name
      check_fetched
      return @name
    end

    def author
      check_fetched
      return @author
    end

    def owner
      check_fetched
      return @owner
    end

    def status
      check_fetched
      return @status
    end

    def priority
      check_fetched
      return @priority
    end

    def projects
      check_fetched
      return @projects
    end

    def date_created
      check_fetched
      return @date_created
    end

    def date_modified
      check_fetched
      return @date_modified
    end
  end
end
