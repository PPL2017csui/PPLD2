require_relative '../models/conduit_lazy_model'

module RbCAW
  # Model class for a Phabricator project. This class is a lazy model,
  # which make it possible to instantiate an object without fetching from
  # the Phabricator host. Additional variable values will be fetched as
  # needed.
  # This class will not fetch additional information from the host unless
  # its variables are accessed.
  #
  # @!attribute [r] id
  #   @return [Fixnum] the size of the list
  #
  # @!attribute [r] name
  #   @return [String] the name of this project
  #
  # @!attribute [r] description
  #   @return [String] the description of this project
  #
  # @!attribute [r] icon
  #   @return [String] the icon of this project
  #
  # @!attribute [r] color
  #   @return [String] the color of this project
  #
  # @!attribute [r] members
  #   @return [Array<UserLazyModel>] the members of this project
  #
  # @!attribute [r] slug
  #   @return [String] the slug (nickname) of this project
  #
  # @!attribute [r] date_created
  #   @return [Fixnum] the date this project was created
  #
  # @!attribute [r] date_modified
  #   @return [Fixnum] the latest date this project was modified
  #
  # @!attribute [r] start_date
  #   @return [Fixnum] the sprint start date of this project
  #
  # @!attribute [r] end_date
  #   @return [Fixnum] the sprint end date of this project
  #
  # @!attribute [r] sprint_info
  #   @return [ProjectSprintInfoLazyModel] the sprint-related information of
  #     this project
  #
  # @!attribute [r] is_sprint
  #   @return [TrueClass, FalseClass] true if this project is an agile project
  #     that uses sprint
  class ProjectLazyModel < ConduitLazyModel
    attr_reader :sprint_info

    # Manually initialize this object by supplying all the instance variables
    #
    # @param id [Fixnum] the size of the list
    # @param name [String] the name of this project
    # @param description [String] the description of this project
    # @param icon [String] the icon of this project
    # @param color [String] the color of this project
    # @param members [Array<UserLazyModel>] the members of this project
    # @param slug [String] the slug (nickname) of this project
    # @param date_created [Fixnum] the date this project was created
    # @param date_modified [Fixnum] the latest date this project was modified
    # @param is_sprint [TrueClass, FalseClass] true if this project is an agile
    #   project that uses sprint
    def manual_init(id, name, description, icon, color, members, slug,
                    date_created, date_modified, is_sprint)
      @id = id
      @name = name
      @description = description
      @icon = icon
      @color = color
      @members = members
      @slug = slug
      @date_created = date_created
      @date_modified = date_modified
      @is_sprint = is_sprint

      @sprint_info = ProjectSprintInfoLazyModel.new(@phid, conduit: @conduit)

      @fetched = true
    end

    # Fetch and assign the values of this project's variables
    def fetch
      project = @conduit.project.get(phid: @phid)

      manual_init(
        project.id, project.name, project.description, project.icon,
        project.color, project.members, project.slug, project.date_created,
        project.date_modified, project.is_sprint
      )
    end

    def id
      check_fetched
      return @id
    end

    def name
      check_fetched
      return @name
    end

    def description
      check_fetched
      return @description
    end

    def icon
      check_fetched
      return @icon
    end

    def color
      check_fetched
      return @color
    end

    def members
      check_fetched
      return @members
    end

    def slug
      check_fetched
      return @slug
    end

    def date_created
      check_fetched
      return @date_created
    end

    def date_modified
      check_fetched
      return @date_modified
    end

    def is_sprint
      check_fetched
      return @is_sprint
    end
  end
end