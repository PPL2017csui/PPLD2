require_relative '../../models/search/cursor'

module RbCAW
  # @abstract Class containing Conduit search result information
  #
  # @attr_reader results [Array<ConduitLazyModel>] Array containing search
  #   results
  # @attr_reader cursor [Cursor] Cursor information for the search result page
  class SearchResult
    attr_reader :results, :cursor
  end
end
