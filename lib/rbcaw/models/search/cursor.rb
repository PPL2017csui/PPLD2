module RbCAW
  # Class containing pagination information for Conduit search result
  #
  # @attr_reader limit [Fixnum]   The maximum amount of objects on the search
  #   result
  # @attr_reader after [Fixnum]   The next search result page
  # @attr_reader before [Fixnum]  The previous search result page
  # @attr_reader order [String]   The ordering of the search result
  class Cursor
    attr_reader :limit, :after, :before, :order

    # @param limit [Fixnum]   the maximum amount of objects on the search result
    # @param after [Fixnum]   the next search result page
    # @param before [Fixnum]  the previous search result page
    # @param order [String]   the ordering of the search result
    def initialize(limit, after, before, order)
      @limit = limit
      @after = after
      @before = before
      @order = order
    end
  end
end
