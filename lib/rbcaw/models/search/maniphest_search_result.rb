require_relative '../../models/task_lazy_model'
require_relative '../../models/user_lazy_model'
require_relative '../../models/project_lazy_model'
require_relative '../../models/search/search_result'

module RbCAW
  # Class containing Maniphest search result information
  class ManiphestSearchResult < SearchResult
    # Parse Maniphest search result in JSON format into proper Ruby classes
    #
    # @param conduit [Conduit] the Conduit instance this parser will use to
    #   fetch additional information
    # @param json [JSON] the Maniphest search result
    def initialize(conduit, json)
      tasks_json = json['result']['data']
      cursor_json = json['result']['cursor']

      @results = []

      tasks_json.each { |t|
        id = t['id']
        phid = t['phid']
        name = t['fields']['name']
        author = UserLazyModel.new(t['fields']['authorPHID'], conduit: conduit)
        owner = UserLazyModel.new(t['fields']['ownerPHID'], conduit: conduit)
        status = t['fields']['status']['value']
        priority = t['fields']['priority']['value']
        date_created = t['fields']['dateCreated']
        date_modified = t['fields']['dateModified']

        begin
          projects = []

          t['attachments']['projects']['projectPHIDs'].each { |project_phid|
            projects.append(
              ProjectLazyModel.new(project_phid, conduit: conduit)
            )
          }
        rescue NoMethodError
          projects = []
        end

        task = TaskLazyModel.new(phid, conduit: conduit)
        task.manual_init(
          id, name, author, owner, status, priority,
          projects, date_created, date_modified
        )

        @results.append(task)
      }

      @cursor = Cursor.new(cursor_json['limit'], cursor_json['after'],
                           cursor_json['before'], cursor_json['order'])
    end
  end
end
