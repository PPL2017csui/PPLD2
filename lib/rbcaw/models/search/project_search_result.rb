require_relative '../../models/search/search_result'
require_relative '../../models/project_lazy_model'
require_relative '../../models/user_lazy_model'


module RbCAW
  # Class containing Project search result information
  class ProjectSearchResult < SearchResult
    # Parse Project search result in JSON format into proper Ruby classes
    #
    # @param conduit [Conduit] the Conduit instance this parser will use to
    #   fetch additional information
    # @param json [JSON] the Project search result
    def initialize(conduit, json)
      projects_json = json['result']['data']
      cursor_json = json['result']['cursor']

      @results = []

      projects_json.each { |p|
        id = p['id']
        phid = p['phid']
        name = p['fields']['name']
        description = p['fields']['description']
        slug = p['fields']['slug']
        icon = p['fields']['icon']['key']
        color = p['fields']['color']['key']
        date_created = p['fields']['dateCreated']
        date_modified = p['fields']['dateModified']
        is_sprint = p['fields']['issprint']

        begin
          members = []

          p['attachments']['members']['members'].each { |member_json|
            member = UserLazyModel.new(member_json['phid'], conduit: conduit)
            members.append(member)
          }
        rescue NoMethodError
          members = []
        end

        project = ProjectLazyModel.new(phid, conduit: conduit)
        project.manual_init(
          id, name, description, icon, color, members, slug,
          date_created, date_modified, is_sprint
        )

        @results.append(project)
      }

      @cursor = Cursor.new(cursor_json['limit'], cursor_json['after'],
                           cursor_json['before'], cursor_json['order'])
    end
  end
end
