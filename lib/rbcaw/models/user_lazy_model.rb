require_relative '../models/conduit_lazy_model'

module RbCAW
  # Model class for a Phabricator user. This class is a lazy model,
  # which make it possible to instantiate an object without fetching from
  # the Phabricator host. Additional variable values will be fetched as
  # needed.
  # This class will not fetch additional information from the host unless
  # its variables are accessed.
  #
  # @!attribute [r] username
  #   @return [String] the username of this user
  #
  # @!attribute [r] realname
  #   @return [String] the realname of this user
  #
  # @!attribute [r] image
  #   @return [String] the image URL of this user
  #
  # @!attribute [r] roles
  #   @return [Array<String>] the roles of this user
  class UserLazyModel < ConduitLazyModel
    # Manually initialize this object by supplying all the instance variables
    #
    # @param username [String] the username of this user
    # @param realname [String] the realname of this user
    # @param image [String] the image URL of this user
    # @param roles [Array<String>] the roles of this user
    def manual_init(username, realname, image, roles)
      @username = username
      @realname = realname
      @image = image
      @roles = roles

      @fetched = true
    end

    # Fetch and assign the values of this user's variables
    def fetch
      user = @conduit.user.get(phid: @phid)

      manual_init(user.username, user.realname, user.image, user.roles)
    end

    def username
      check_fetched
      return @username
    end

    def realname
      check_fetched
      return @realname
    end

    def image
      check_fetched
      return @image
    end

    def roles
      check_fetched
      return @roles
    end
  end
end