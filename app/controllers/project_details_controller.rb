require 'rbcaw/conduit'

class ProjectDetailsController < ApplicationController
  def index
    id = params['id']
    conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

    @project = conduit.project.search(constraints: {'ids' => [id.to_i]}).results[0]

  end
end
