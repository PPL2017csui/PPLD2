require 'rbcaw/conduit'
require 'date'

class HomeController < ApplicationController
  def index
    conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

    begin
      @project_list = conduit.project.search.results
      @task_list = conduit.maniphest.search(attachments:{'projects':true}).results

      @task_by_project = {}
      @project_list.each{ |project|
        @task_by_project[project.phid] = {}
        @task_by_project[project.phid]['num_complete'] = 0
        @task_by_project[project.phid]['num_incomplete'] = 0
        @task_by_project[project.phid]['percentage'] = 0.0
      }

      @task_list.each{ |task|
        task.projects.each{ |p_task|
          if @task_by_project.key?(p_task.phid)
            if task.sprint_info.is_closed
              @task_by_project[p_task.phid]['num_complete'] += 1
            else
              @task_by_project[p_task.phid]['num_incomplete'] += 1
            end
          end
        }
      }

      @project_data = {}
      @project_data['num_project'] = @project_list.length
      @project_data['projects'] = []
      @project_data['num_finished_project'] = 0

      @project_list.each{|project|
        task_data = @task_by_project[project.phid]
        is_finished = false
        if task_data['num_complete'] > 0 and task_data['num_incomplete'] == 0
          is_finished = true
          @project_data['num_finished_project'] += 1
        end

        percentage = 0
        if task_data['num_complete'] + task_data['num_incomplete'] > 0
          percentage = task_data['num_complete'] * 100.0 / (task_data['num_complete'] + task_data['num_incomplete'])
        end

        data = {
            phid: project.phid,
            name: project.name,
            start_date: Time.at(project.sprint_info.start_date).to_time.to_s(:date_format),
            end_date: Time.at(project.sprint_info.end_date).to_time.to_s(:date_format),
            is_finished: is_finished,
            percentage: percentage,
            num_task_complete: task_data['num_complete'],
            num_task_incomplete: task_data['num_incomplete'],
            num_task: task_data['num_complete'] + task_data['num_incomplete'],
            phabricator_url: "#{CONDUIT['host']}project/view/#{project.id}"
        }
        @project_data['projects'].append(data)
      }

      @project_data['num_ongoing_project'] = @project_data['num_project'] - @project_data['num_finished_project']

    rescue RbCAW::ConnectionError
      @error_message = 'Error connecting to Phabricator'
    rescue
      @error_message = 'Unknown Error'

    end

  end
end

