import React from 'react';
import TestUtils from 'react-addons-test-utils';
import ProjectCard from '../ProjectCard';

const project1 = {
  name: 'Project 1',
  start_date: '2017-03-28',
  end_date: '2017-04-28',
  percentage: 80,
};

const project2 = {
  name: 'Project 2',
  start_date: '2017-03-28',
  end_date: '2017-04-28',
  percentage: 80.33333,
};

describe('project list', () => {
  test('renders a project correctly', () => {
    const shallowRenderer = TestUtils.createRenderer();
    shallowRenderer.render(React.createElement(ProjectCard, project1));
    const projectCard = shallowRenderer.getRenderOutput();
    const row = projectCard.props.children;
    const firstCol = row.props.children[0];
    const title = firstCol.props.children[0];

    expect(TestUtils.isElement(projectCard)).toBe(true);
    expect(title.props.children).toEqual(project1.name);
  });

  test('renders a percentage project correctly', () => {
    const shallowRenderer = TestUtils.createRenderer();
    shallowRenderer.render(React.createElement(ProjectCard, project2));
    const projectCard = shallowRenderer.getRenderOutput();
    const row = projectCard.props.children;
    const secondCol = row.props.children[1];
    const percentage = secondCol.props.children[1];

    expect(percentage.props.children[0]).toEqual(project2.percentage.toFixed(2));
  });
});
