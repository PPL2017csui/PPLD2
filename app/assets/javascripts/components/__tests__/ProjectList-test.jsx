import React from 'react';
import TestUtils from 'react-addons-test-utils';
import ProjectList from '../ProjectList';

const projectNames = ['Project 1', 'Project 2'];
const projects = [
  { name: 'Project 1', phid: '1', start_date: '2017-03-28', end_date: '2017-04-28' },
  { name: 'Project 2', phid: '2', start_date: '2017-04-28', end_date: '2017-05-28' },
];

describe('project list', () => {
  test('renders projects correctly', () => {
    const shallowRenderer = TestUtils.createRenderer();
    shallowRenderer.render(React.createElement(ProjectList, { name, projects }));
    const projectList = shallowRenderer.getRenderOutput();
    const projectCards = projectList.props.children;

    expect(projectCards.map(projectCard =>
      projectCard.props.name,
    )).toEqual(projectNames);
  });
});
