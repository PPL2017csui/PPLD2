import React from 'react';
import ProjectCard from './ProjectCard.jsx';

export default class ProjectList extends React.Component {
  render() {
    const projectList = this.props.projects.map(project => (
      <ProjectCard
        name={project.name}
        start_date={project.start_date}
        end_date={project.end_date}
        percentage={project.percentage}
        phabricator_url={project.phabricator_url}
        key={project.phid}
      />
    ));

    return (
      <div>
        {projectList}
      </div>
    );
  }
}

ProjectList.propTypes = {
  projects: React.PropTypes.arrayOf(React.PropTypes.object),
};
ProjectList.defaultProps = {
  projects: null,
};
