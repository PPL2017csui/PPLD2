import Doughnut from 'react-chartjs-2';
import React from 'react';
import ColorPercentage from '../utilities/ColorPercentage.jsx';

export default class ProjectCard extends React.Component {
  render() {
    let completedPercentage = this.props.percentage;
    if (completedPercentage !== parseInt(completedPercentage, 10)) {
      completedPercentage = completedPercentage.toFixed(2);
    }
    let uncompletedPercentage = 100 - completedPercentage;
    if (uncompletedPercentage !== parseInt(uncompletedPercentage, 10)) {
      uncompletedPercentage = uncompletedPercentage.toFixed(2);
    }
    const chartColor = ColorPercentage.getColor(completedPercentage / 100);

    const data = {
      labels: [
        'completed',
        'uncompleted',
      ],
      datasets: [
        {
          data: [
            completedPercentage,
            uncompletedPercentage,
          ],
          backgroundColor: [
            chartColor,
            '#bebebe',
          ],
          hoverBackgroundColor: [
            chartColor,
            '#bebebe',
          ],
        },
      ],
    };

    const options = {
      elements: {
        arc: {
          borderWidth: 0,
        },
      },
      cutoutPercentage: 80,
      legend: {
        display: false,
      },
      maintainAspectRatio: false,
      responsive: true,
    };

    return (
      <div className="card">
        <div className="row">
          <div className="col col--9">
            <h2>{this.props.name}</h2>
            <p className="card__text">
              Start: {this.props.start_date}
            </p>
            <p className="card__text">
              End: {this.props.end_date}
            </p>
            <p className="card__list-link">
              <a href={this.props.phabricator_url} target="_blank" rel="noopener noreferrer">
                Phabricator
              </a>
            </p>
          </div>
          <div className="col col--3 u__valign--middle u__position--relative">
            <Doughnut data={data} options={options} width={120} height={120} />
            <div className="doughnut__legend">
              {completedPercentage}%
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProjectCard.propTypes = {
  name: React.PropTypes.string,
  start_date: React.PropTypes.string,
  end_date: React.PropTypes.string,
  percentage: React.PropTypes.number,
  phabricator_url: React.PropTypes.string,
};

ProjectCard.defaultProps = {
  name: '',
  start_date: '',
  end_date: '',
  percentage: 0,
  phabricator_url: '#',
};
