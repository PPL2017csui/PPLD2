import React from 'react';

export default class DropdownItem extends React.Component {
  render() {
    return (
      <li>
        <a href={this.props.link}>
          {this.props.text}
        </a>
      </li>
    );
  }
}

DropdownItem.propTypes = {
  text: React.PropTypes.string,
  link: React.PropTypes.string,
};

DropdownItem.defaultProps = {
  text: '',
  link: '#',
};
