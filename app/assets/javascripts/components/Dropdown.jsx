import onClickOutside from 'react-onclickoutside';
import React from 'react';
import DropdownButton from './DropdownButton.jsx';
import DropdownItem from './DropdownItem.jsx';

class Dropdown extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);

    this.state = {
      opened: false,
    };
  }

  handleClick() {
    this.setState(
      {
        opened: !this.state.opened,
      },
    );
  }

  handleClickOutside() {
    this.setState(
      {
        opened: false,
      },
    );
  }

  render() {
    const content = this.state.opened ? (
      <ul className="dropdown__menu">
        {this.props.menuItems.map(item =>
          <DropdownItem key={item.link} text={item.text} link={item.link} />,
        )}
      </ul>) : null;

    return (
      <div className="dropdown">
        <DropdownButton
          active={this.state.opened}
          title={this.props.title}
          onClick={this.handleClick}
        />
        {content}
      </div>
    );
  }

}

Dropdown.propTypes = {
  menuItems: React.PropTypes.arrayOf(React.PropTypes.object),
  title: React.PropTypes.string,
};

Dropdown.defaultProps = {
  menuItems: [],
  title: 'Dropdown',
};

export default onClickOutside(Dropdown);
