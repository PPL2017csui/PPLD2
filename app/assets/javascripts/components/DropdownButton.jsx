import classNames from 'classnames';
import React from 'react';

export default class DropdownButton extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick();
  }

  render() {
    const classes = classNames({
      dropdown__button: true,
      active: this.props.active,
    });

    return (
      <button className={classes} onClick={this.handleClick}>
        {this.props.title} <i className="fa fa-caret-down" />
      </button>
    );
  }
}

DropdownButton.propTypes = {
  title: React.PropTypes.string,
  active: React.PropTypes.bool,
  onClick: React.PropTypes.func.isRequired,
};

DropdownButton.defaultProps = {
  title: 'Dropdown',
  active: false,
};
