Rails.application.routes.draw do
  get 'project/:id', to: 'project_details#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
end
